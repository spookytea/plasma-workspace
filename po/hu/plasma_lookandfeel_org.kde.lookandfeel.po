# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2014, 2015, 2019.
# Kiszel Kristóf <kiszel.kristof@gmail.com>, 2017, 2018, 2020, 2021.
# SPDX-FileCopyrightText: 2021, 2023, 2024 Kristof Kiszel <ulysses@fsf.hu>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-02-05 22:54+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Billentyűzetkiosztás: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Felhasználónév"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Jelszó"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Bejelentkezés"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "A Caps Lock be van kapcsolva"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Alvó állapot"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "Újraindítás"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "Leállítás"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Egyéb…"

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "Adja meg a felhasználónevet és a jelszót"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "Felhasználólista"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Virtuális billentyűzet"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "Sikertelen bejelentkezés"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Asztali munkamenet: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Óra:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Maradjon látható a feloldóképernyő eltűnésekor"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Médiavezérlők:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Megjelenítés a feloldóképernyőn"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Sikertelen feloldás"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "Alvó állapot"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "Hibernálás"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "Felhasználóváltás"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Kiosztásváltás"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Feloldás"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(vagy használja az ujjlenyomatolvasót)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(vagy használja intelligens kártyáját)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Nincs cím"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Nincs médialejátszás"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Előző szám"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Média indítása/szüneteltetése"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Következő szám"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "Szoftverfrissítések telepítése és újraindítás 1 másodperc múlva"
msgstr[1] "Szoftverfrissítések telepítése és újraindítás %1 másodperc múlva"

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Újraindítás 1 másodperc múlva"
msgstr[1] "Újraindítás %1 másodperc múlva"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Kijelentkezés 1 másodperc múlva"
msgstr[1] "Kijelentkezés %1 másodperc múlva"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Újraindítás 1 másodperc múlva"
msgstr[1] "Leállítás %1 másodperc múlva"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Másik felhasználó is bejelentkezve van. Ha a számítógépet leállítja vagy "
"újraindítja, az ő munkája elveszhet."
msgstr[1] ""
"Más felhasználók (%1) is bejelentkezve vannak. Ha a számítógépet leállítja "
"vagy újraindítja, az ő munkájuk elveszhet."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr "Újraindításkor a számítógép a firmware-telepítő képernyőre fog lépni."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "Alvó állapot most"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "Hibernálás most"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Frissítés és újraindítás"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "Frissítés és újraindítás most"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "Újraindítás most"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "Leállítás most"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "Kijelentkezés"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "Kijelentkezés most"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "Mégsem"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma, a KDE-től"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Switch to This Session"
#~ msgstr "Váltás erre a munkamenetre"

#~ msgid "Start New Session"
#~ msgstr "Új munkamenet indítása"

#~ msgid "Back"
#~ msgstr "Vissza"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "Akkumulátor: %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Nem használt"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "ezen: TTY%1 (Kijelző: %2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "Configure"
#~ msgstr "Beállítás"

#, fuzzy
#~| msgid "Configure KRunner…"
#~ msgid "Configure KRunner Behavior"
#~ msgstr "A KRunner beállításai…"

#~ msgid "Configure KRunner…"
#~ msgstr "A KRunner beállításai…"

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Keresés: „%1”…"

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Keresés…"

#~ msgid "Show Usage Help"
#~ msgstr "Használati súgó megjelenítése"

#~ msgid "Pin"
#~ msgstr "Rögzítés"

#~ msgid "Pin Search"
#~ msgstr "Keresés rögzítése"

#~ msgid "Keep Open"
#~ msgstr "Tartsa nyitva"

#~ msgid "Recent Queries"
#~ msgstr "Legutóbbi lekérdezések"

#~ msgid "Remove"
#~ msgstr "Eltávolítás"

#~ msgid "in category recent queries"
#~ msgstr "a legutóbbi lekérdezések kategóriában"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "Megjelenítés:"

#~ msgid "Configure Search Plugins"
#~ msgstr "Kereső bővítmények beállítása"

#~ msgctxt ""
#~ "This is the first text the user sees while starting in the splash screen, "
#~ "should be translated as something short, is a form that can be seen on a "
#~ "product. Plasma is the project name so shouldn't be translated."
#~ msgid "Plasma 25th Anniversary Edition by KDE"
#~ msgstr "A Plasma 25. évfordulós kiadása a KDE-től"

#~ msgid "Close"
#~ msgstr "Bezárás"

#~ msgctxt "verb, to show something"
#~ msgid "Always show"
#~ msgstr "Mindig látszódjon"

#~ msgid "Suspend"
#~ msgstr "Felfüggesztés"

#~ msgid "Reboot"
#~ msgstr "Újraindítás"

#~ msgid "Logout"
#~ msgstr "Kijelentkezés"

#~ msgid "Different User"
#~ msgstr "Másik felhasználó"

#, fuzzy
#~| msgid "Login as different user"
#~ msgid "Log in as a different user"
#~ msgstr "Bejelentkezés másik felhasználóval"

#, fuzzy
#~| msgid "Password"
#~ msgid "Password..."
#~ msgstr "Jelszó"

#~ msgid "Login"
#~ msgstr "Bejelentkezés"

#~ msgid "Shutdown"
#~ msgstr "Kikapcsolás"

#~ msgid "Switch"
#~ msgstr "Váltás"

#~ msgid "New Session"
#~ msgstr "Új munkamenet"

#~ msgid "%1%. Charging"
#~ msgstr "%1%. Töltés"

#~ msgid "Fully charged"
#~ msgstr "Teljesen feltöltött"

#~ msgid "%1% battery remaining"
#~ msgstr "%1% van hátra"

#~ msgid "Change Session"
#~ msgstr "Munkamenetváltás"

#~ msgid "Create Session"
#~ msgstr "Munkamenet létrehozása"

#~ msgid "Change Session..."
#~ msgstr "Munkamenetváltás…"

#, fuzzy
#~| msgid "%1 (%2)"
#~ msgctxt "Username (logged in on console)"
#~ msgid "%1 (TTY)"
#~ msgstr "%1 (%2)"

#~ msgctxt "Button to restart the computer"
#~ msgid "Reboot"
#~ msgstr "Újraindítás"

#, fuzzy
#~| msgid "Shut down"
#~ msgctxt "Button to shut down the computer"
#~ msgid "Shut down"
#~ msgstr "Leállítás"

#, fuzzy
#~| msgid "Logging out"
#~ msgctxt "Dialog heading, confirm log out, not a status label"
#~ msgid "Logging out"
#~ msgstr "Kijelentkezés"

#, fuzzy
#~| msgid "Rebooting"
#~ msgctxt "Dialog heading, confirm reboot, not a status label"
#~ msgid "Rebooting"
#~ msgstr "Újraindítás"

#, fuzzy
#~| msgid "%1 (%2)"
#~ msgctxt "Username (on display number)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"
