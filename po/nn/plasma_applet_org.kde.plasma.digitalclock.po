# Translation of plasma_applet_org.kde.plasma.digitalclock to Norwegian Nynorsk
#
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2018, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-15 00:39+0000\n"
"PO-Revision-Date: 2024-03-09 18:13+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/config/config.qml:19
#, kde-format
msgid "Appearance"
msgstr "Utsjånad"

#: package/contents/config/config.qml:24
#, kde-format
msgid "Calendar"
msgstr "Kalender"

#: package/contents/config/config.qml:29
#: package/contents/ui/CalendarView.qml:432
#, kde-format
msgid "Time Zones"
msgstr "Tidssoner"

#: package/contents/ui/CalendarView.qml:144
#, kde-format
msgid "Events"
msgstr "Hendingar"

#: package/contents/ui/CalendarView.qml:153
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr "Legg til …"

#: package/contents/ui/CalendarView.qml:157
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr "Legg til ny hending"

#: package/contents/ui/CalendarView.qml:392
#, kde-format
msgid "No events for today"
msgstr "Ingen hendingar i dag"

#: package/contents/ui/CalendarView.qml:393
#, kde-format
msgid "No events for this day"
msgstr "Ingen hendingar denne datoen"

#: package/contents/ui/CalendarView.qml:443
#, kde-format
msgid "Switch…"
msgstr "Byt …"

#: package/contents/ui/CalendarView.qml:444
#: package/contents/ui/CalendarView.qml:447
#, kde-format
msgid "Switch to another timezone"
msgstr "Byt til anna tidssone"

#: package/contents/ui/configAppearance.qml:48
#, kde-format
msgid "Information:"
msgstr "Informasjon:"

#: package/contents/ui/configAppearance.qml:52
#, kde-format
msgid "Show date"
msgstr "Vis dato"

#: package/contents/ui/configAppearance.qml:60
#, kde-format
msgid "Adaptive location"
msgstr "Dynamisk plassering"

#: package/contents/ui/configAppearance.qml:61
#, kde-format
msgid "Always beside time"
msgstr "Alltid ved sida av klokkeslettet"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgid "Always below time"
msgstr "Alltid under klokkeslettet"

#: package/contents/ui/configAppearance.qml:70
#, kde-format
msgid "Show seconds:"
msgstr "Vis sekund:"

#: package/contents/ui/configAppearance.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Never"
msgstr "Aldri"

#: package/contents/ui/configAppearance.qml:73
#, kde-format
msgctxt "@option:check"
msgid "Only in the tooltip"
msgstr "Berre i hjelpebobla"

#: package/contents/ui/configAppearance.qml:74
#: package/contents/ui/configAppearance.qml:94
#, kde-format
msgid "Always"
msgstr "Alltid"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "Show time zone:"
msgstr "Vis tidssone:"

#: package/contents/ui/configAppearance.qml:89
#, kde-format
msgid "Only when different from local time zone"
msgstr "Berre når ho er ulik den lokale tidssona"

#: package/contents/ui/configAppearance.qml:103
#, kde-format
msgid "Display time zone as:"
msgstr "Vis tidssona som:"

#: package/contents/ui/configAppearance.qml:108
#, kde-format
msgid "Code"
msgstr "Kode"

#: package/contents/ui/configAppearance.qml:109
#, kde-format
msgid "City"
msgstr "By"

#: package/contents/ui/configAppearance.qml:110
#, kde-format
msgid "Offset from UTC time"
msgstr "UTC-avstand"

#: package/contents/ui/configAppearance.qml:122
#, kde-format
msgid "Time display:"
msgstr "Tidsvising:"

#: package/contents/ui/configAppearance.qml:127
#, kde-format
msgid "12-Hour"
msgstr "12-timars ur"

#: package/contents/ui/configAppearance.qml:128
#: package/contents/ui/configCalendar.qml:51
#, kde-format
msgid "Use Region Defaults"
msgstr "Bruk regionsinnstillingane"

#: package/contents/ui/configAppearance.qml:129
#, kde-format
msgid "24-Hour"
msgstr "24-timars ur"

#: package/contents/ui/configAppearance.qml:136
#, kde-format
msgid "Change Regional Settings…"
msgstr "Endra regionale innstillingar …"

#: package/contents/ui/configAppearance.qml:147
#, kde-format
msgid "Date format:"
msgstr "Datoformat:"

#: package/contents/ui/configAppearance.qml:155
#, kde-format
msgid "Long Date"
msgstr "Lang dato"

#: package/contents/ui/configAppearance.qml:162
#, kde-format
msgid "Short Date"
msgstr "Kort dato"

#: package/contents/ui/configAppearance.qml:169
#, kde-format
msgid "ISO Date"
msgstr "ISO-dato"

#: package/contents/ui/configAppearance.qml:176
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr "Tilpassa"

#: package/contents/ui/configAppearance.qml:206
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Dokumentasjon på tidsformat</a>"

#: package/contents/ui/configAppearance.qml:230
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr "Tekst som skal visast:"

#: package/contents/ui/configAppearance.qml:232
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr "Automatisk"

#: package/contents/ui/configAppearance.qml:236
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr ""
"Teksten vil bruka systemskrifta og vert automatisk utvida til å fylla all "
"ledig plass."

#: package/contents/ui/configAppearance.qml:246
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr "Manuelt"

#: package/contents/ui/configAppearance.qml:256
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr "Vel stil …"

#: package/contents/ui/configAppearance.qml:269
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr "%1 pt %2"

#: package/contents/ui/configAppearance.qml:277
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr "Vel skrift"

#: package/contents/ui/configCalendar.qml:38
#, kde-format
msgid "General:"
msgstr "Generelt:"

#: package/contents/ui/configCalendar.qml:39
#, kde-format
msgid "Show week numbers"
msgstr "Vis vekenummer"

#: package/contents/ui/configCalendar.qml:44
#, kde-format
msgid "First day of week:"
msgstr "Første dagen i veka:"

#: package/contents/ui/configCalendar.qml:65
#, kde-format
msgid "Available Plugins:"
msgstr "Tilgjengelege programtillegg:"

#: package/contents/ui/configTimeZones.qml:42
#, kde-format
msgid ""
"Tip: if you travel frequently, add your home time zone to this list. It will "
"only appear when you change the systemwide time zone to something else."
msgstr ""
"Tips: Viss du ofte er på reisefot, legg til ei oppføring for tidssona til "
"heimstaden din her. Denne vert då berre vist når du byter tidssona for heile "
"systemet til ei anna tidssone."

#: package/contents/ui/configTimeZones.qml:79
#, kde-format
msgid "Clock is currently using this time zone"
msgstr "Klokka brukar denne tidssona"

#: package/contents/ui/configTimeZones.qml:81
#, kde-format
msgctxt ""
"@label This list item shows a time zone city name that is identical to the "
"local time zone's city, and will be hidden in the timezone display in the "
"plasmoid's popup"
msgid "Hidden while this is the local time zone's city"
msgstr "Vert gøymd viss dette er byen til den lokale tidssona"

#: package/contents/ui/configTimeZones.qml:106
#, kde-format
msgid "Switch Systemwide Time Zone…"
msgstr "Byt tidssone for heile systemet …"

#: package/contents/ui/configTimeZones.qml:118
#, kde-format
msgid "Remove this time zone"
msgstr "Fjern denne tidssona"

#: package/contents/ui/configTimeZones.qml:128
#, kde-format
msgid "Systemwide Time Zone"
msgstr "Tidssone for heile systemet"

#: package/contents/ui/configTimeZones.qml:128
#, kde-format
msgid "Additional Time Zones"
msgstr "Fleire tidssoner"

#: package/contents/ui/configTimeZones.qml:141
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""
"Legg til fleire tidssoner for å visa dei alle i sprettopp­vindauget til skjerm­"
"elementet, eller bruk éi av dei for klokka."

#: package/contents/ui/configTimeZones.qml:150
#, kde-format
msgid "Add Time Zones…"
msgstr "Legg til tidssoner …"

#: package/contents/ui/configTimeZones.qml:160
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr "Byt vist tidssone ved bruk av musehjulet på klokka"

#: package/contents/ui/configTimeZones.qml:167
#, kde-format
msgid ""
"Using this feature does not change the systemwide time zone. When you "
"travel, switch the systemwide time zone instead."
msgstr ""
"Denne funksjonen endrar ikkje tidssona for heile systemet. Når du reiser, "
"byt heller den tidssona."

#: package/contents/ui/configTimeZones.qml:190
#, kde-format
msgid "Add More Timezones"
msgstr "Legg til fleire tidssoner"

#: package/contents/ui/configTimeZones.qml:202
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local timezone was enabled "
"automatically."
msgstr ""
"Du må velja minst éi tidssone. Den lokale tidssona di vart automatisk valt."

#: package/contents/ui/configTimeZones.qml:238
#, kde-format
msgid "%1, %2 (%3)"
msgstr "%1, %2 (%3)"

#: package/contents/ui/configTimeZones.qml:240
#, kde-format
msgid "%1, %2"
msgstr "%1, %2"

#: package/contents/ui/main.qml:148
#, kde-format
msgid "Copy to Clipboard"
msgstr "Kopier til utklippstavla"

#: package/contents/ui/main.qml:152
#, kde-format
msgid "Adjust Date and Time…"
msgstr "Still inn dato og klokkeslett …"

#: package/contents/ui/main.qml:158
#, kde-format
msgid "Set Time Format…"
msgstr "Vel klokkeslettformat …"

#: package/contents/ui/Tooltip.qml:31
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr "I dag er %1"

#: package/contents/ui/Tooltip.qml:135
#, kde-format
msgctxt "@label %1 is a city or time zone name"
msgid "%1:"
msgstr "%1:"

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr "Andre kalendrar"

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr "%1 (UNIX-tid)"

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr "%1 (juliansk dato)"

#: plugin/timezonemodel.cpp:147
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr "Lokal"

#: plugin/timezonemodel.cpp:149
#, kde-format
msgid "System's local time zone"
msgstr "Systemets lokale tidssone"
